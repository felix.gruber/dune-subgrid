#ifndef DUNE_SUBGRID_INDEXSETS_HH
#define DUNE_SUBGRID_INDEXSETS_HH

/** \file
* \brief The index and id sets for the SubGrid class
*/

#include <vector>
#include <dune/geometry/referenceelements.hh>

namespace Dune {


/** \todo Take the index types from the host grid */
template<class GridImp>
class SubGridLevelIndexSet :
    public IndexSetDefaultImplementation<GridImp,SubGridLevelIndexSet<GridImp> >
{
    public:

        typedef typename std::remove_const<GridImp>::type::HostGridType HostGrid;
        typedef typename HostGrid::Traits::GlobalIdSet HostIdSet;
        typedef typename HostIdSet::IdType HostIdType;

        enum {dim = GridImp::dimension};

        typedef IndexSetDefaultImplementation<GridImp,SubGridLevelIndexSet<GridImp> > Base;

        /** \brief Default constructor
        * Unfortunately we can't force the user to init grid_ and level_, because
        * UGGridLevelIndexSets are meant to be stored in an array.
        *
        */
        SubGridLevelIndexSet ()
        {}


        //! get index of an entity
        template<int codim>
        int index (const typename GridImp::Traits::template Codim<codim>::Entity& e) const
        {
            return grid_->indexStorage.template levelIndex<codim>(e, level_);
        }


        //! get index of subEntity of a codim 0 entity
        template<int cc>
        int subIndex (const typename GridImp::Traits::template Codim<cc>::Entity& e, int i, unsigned int codim) const
        {
            if (cc!=0)
                DUNE_THROW( NotImplemented, "subIndex for higher codimension entity not implemented for SubGrid." );

            return grid_->indexStorage.template levelSubIndex<cc>(e, i, level_, codim);
        }


        //! get number of entities of given codim, type and on this level
        int size (int codim) const {
            return grid_->indexStorage.levelSize(codim, level_);
        }


        //! get number of entities of given codim, type and on this level
        int size (GeometryType type) const
        {
            return grid_->indexStorage.levelSize(type, level_);
        }

        /** \brief Deliver all geometry types used in this grid */
        const std::vector<GeometryType>& types(int codim) const
        {
            return grid_->indexStorage.levelGeomTypes(codim, level_);
        }


        /** @brief Return true if the given entity is contained in \f$E\f$.
         */
        template<class EntityType>
        bool contains (const EntityType& e) const
        {
            return grid_ == &(e.impl().grid())
                && level_ == e.impl().hostEntity().level();
        }


        /** \todo Currently we support only vertex and element indices */
        void update(const GridImp& grid, int level)
        {
            grid_ = &grid;
            level_ = level;
        }


        GridImp* grid_;

        int level_;
};




template<class GridImp>
class SubGridLeafIndexSet :
    public IndexSetDefaultImplementation<GridImp,SubGridLeafIndexSet<GridImp> >
{
    public:

        typedef typename std::remove_const<GridImp>::type::HostGridType HostGrid;
        typedef typename HostGrid::Traits::GlobalIdSet HostIdSet;
        typedef typename HostIdSet::IdType HostIdType;

        /*
        * We use the remove_const to extract the Type from the mutable class,
        * because the const class is not instantiated yet.
        */
        enum {dim = std::remove_const<GridImp>::type::dimension};

        typedef IndexSetDefaultImplementation<GridImp,SubGridLeafIndexSet<GridImp> > Base;


        //! constructor stores reference to a grid and level
        SubGridLeafIndexSet ()
        {}


        //! get index of an entity
        /*
            We use the remove_const to extract the Type from the mutable class,
            because the const class is not instantiated yet.
        */
        template<int codim>
        int index (const typename std::remove_const<GridImp>::type::Traits::template Codim<codim>::Entity& e) const
        {
            return grid_->indexStorage.template leafIndex<codim>(e);
        }


        //! get index of subEntity of a codim 0 entity
        template<int cc>
        int subIndex (const typename std::remove_const<GridImp>::type::Traits::template Codim<cc>::Entity& e, int i, unsigned int codim) const
        {
            if (cc != 0)
                DUNE_THROW( NotImplemented, "subIndex for higher codimension entity not implemented for SubGrid." );

            return grid_->indexStorage.template leafSubIndex<cc>(e, i, codim);
        }


        //! get number of entities of given type
        int size (GeometryType type) const
        {
            return grid_->indexStorage.leafSize(type);
        }


        //! get number of entities of given codim
        int size (int codim) const
        {
            return grid_->indexStorage.leafSize(codim);
        }

        /** \brief Deliver all geometry types used in this grid */
        const std::vector<GeometryType>& types (int codim) const
            {
                return grid_->indexStorage.leafGeomTypes(codim);
            }


        /** @brief Return true if the given entity is contained in \f$E\f$.
         */
        template<class EntityType>
        bool contains (const EntityType& e) const
        {
            enum { codim = EntityType::codimension };
            return (grid_->indexStorage.template leafIndex<codim>(e)>=0);
        }

        /** \todo Currently we support only vertex and element indices */
        void update(const GridImp& grid)
        {
            grid_ = &grid;
        }


        GridImp* grid_;
};




template <class GridImp>
class SubGridGlobalIdSet :
    public IdSet<GridImp,SubGridGlobalIdSet<GridImp>,
        typename std::remove_const<GridImp>::type::HostGridType::Traits::GlobalIdSet::IdType>
{
    private:

        typedef typename std::remove_const<GridImp>::type::HostGridType HostGrid;

        // Why do we need remove_const here ?
        enum {dim = std::remove_const<GridImp>::type::dimension};

    public:
        //! constructor stores reference to a grid
        SubGridGlobalIdSet (const GridImp& g) : grid_(&g) {}

        //! define the type used for persistent indices
        typedef typename HostGrid::Traits::GlobalIdSet::IdType GlobalIdType;


        //! get id of an entity
        /*
        We use the remove_const to extract the Type from the mutable class,
        because the const class is not instantiated yet.
        */
        template<int cd>
        GlobalIdType id (const typename std::remove_const<GridImp>::type::Traits::template Codim<cd>::Entity& e) const
        {
            // Return id of the host entity
            return grid_->getHostGrid().globalIdSet().id(e.impl().hostEntity());
        }


        //! get id of subEntity
        /*
            We use the remove_const to extract the Type from the mutable class,
            because the const class is not instantiated yet.
        */
        GlobalIdType subId (const typename std::remove_const<GridImp>::type::Traits::template Codim<0>::Entity& e, int i, unsigned int cc) const
        {
            // Return sub id of the host entity
            return grid_->getHostGrid().globalIdSet().subId(e.impl().hostEntity(),i,cc);
        }


        /** \todo Should be private */
        void update() {}


        const GridImp* grid_;
};




template<class GridImp>
class SubGridLocalIdSet :
    public IdSet<GridImp,SubGridLocalIdSet<GridImp>,
        typename std::remove_const<GridImp>::type::HostGridType::Traits::LocalIdSet::IdType>
{
    private:

        typedef typename std::remove_const<GridImp>::type::HostGridType HostGrid;

        // Why do we need remove_const here ?
        enum {dim = std::remove_const<GridImp>::type::dimension};

    public:
        //! define the type used for persistent local ids
        typedef typename HostGrid::Traits::LocalIdSet::IdType LocalIdType;


        //! constructor stores reference to a grid
        SubGridLocalIdSet (const GridImp& g) : grid_(&g) {}


        //! get id of an entity
        /*
            We use the remove_const to extract the Type from the mutable class,
            because the const class is not instantiated yet.
        */
        template<int cd>
        LocalIdType id (const typename std::remove_const<GridImp>::type::Traits::template Codim<cd>::Entity& e) const
        {
            // Return id of the host entity
            return grid_->getHostGrid().localIdSet().id(e.impl().hostEntity());
        }


        //! get id of subEntity
        /*
        * We use the remove_const to extract the Type from the mutable class,
        * because the const class is not instantiated yet.
        */
        LocalIdType subId (const typename std::remove_const<GridImp>::type::Traits::template Codim<0>::Entity& e, int i, unsigned int cc) const
        {
            // Return sub id of the host entity
            return grid_->getHostGrid().localIdSet().subId(e.impl().hostEntity(),i,cc);
        }


        /** \todo Should be private */
        void update() {}


        const GridImp* grid_;
};


}  // namespace Dune


#endif
