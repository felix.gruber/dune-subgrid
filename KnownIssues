/**
\page KnownIssues Known Issues

The following issues are known when using dune-subgrid:
<ul>
    <li>
        LeafIntersectionIterator does not implement numerInNeighbor(),
        intersectionXxxLocal().
    </li>
    <li>
        Currently only grids with elements of a single geometry work as hostgrids.
    </li>
    <li>
        LeafIntersectionIterator can return wrong intersections if working with dune-grid1.3svn Revision &lt; 5101  
    </li>
    <li>
        creating the (leaf-) end iterator can be costly (especially with ALU3dSimplexGrid). For a walk over all 
        (leaf-) entities we strongly(!) recommend creating the end iterator outside the loop:
        Example:
        <code>
            ElementLeafIterator end=grid.leafend<0>();
            for (ElementLeafIterator eltIt=grid.leafbegin<0>(); eltIt!=end; ++eltIt)
            {...}
        </code>
    </li>
</ul>
Subgrid uses certain host grid features where you might not expect it.
If a subgrid behaves strange this might be caused by bugs in the host grid
you normally don't see. In this case you can define the macro
SUBGRID_VERBOSE to get more output and use the subgrid method report()
to check the subgrid indices.

*/
